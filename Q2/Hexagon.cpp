#include "Hexagon.h"
Hexagon::Hexagon(std::string col, std::string nam, double lenght) : Shape(nam, col)
{
	if (lenght < 0)
	{
		throw shapeException();
	}
	this->_lenght = lenght;
}
Hexagon::~Hexagon() {}
double Hexagon::CalArea()
{
	return MathUtils::calHexagonArea(this->_lenght);
}
void Hexagon::draw()
{
	std::cout << getName() << std::endl << getColor() << std::endl << "lenght is: " << _lenght << std::endl << "area is: " << CalArea() << std::endl;
}
void Hexagon::setLenght(double lenght)
{
	if (lenght < 0)
	{
		throw shapeException();
	}
	this->_lenght = lenght;
}