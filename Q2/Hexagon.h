#pragma once
#include "shape.h"
#include "mathUtils.h"
#include "shapeexception.h"
class Hexagon : public Shape
{
public:
	Hexagon(std::string col, std::string nam, double lenght);
	virtual ~Hexagon();
	void draw();
	double CalArea();
	void setLenght(double lenght);
private:
	double _lenght;
};