#include "Pentagon.h"
Pentagon::Pentagon(std::string col, std::string nam, double lenght) : Shape(nam, col)
{
	if (lenght < 0)
	{
		throw shapeException();
	}
	this->_lenght = lenght;
}
Pentagon::~Pentagon() {}
double Pentagon::CalArea()
{
	return MathUtils::calPentagonArea(this->_lenght);
}
void Pentagon::draw()
{
	std::cout << getName() << std::endl << getColor() << std::endl << "lenght is: " << _lenght << std::endl << "area is: " << CalArea() << std::endl;
}
void Pentagon::setLenght(double lenght)
{
	if (lenght < 0)
	{
		throw shapeException();
	}
	this->_lenght = lenght;
}