#pragma once
#include "shape.h"
#include "mathUtils.h"
#include "shapeexception.h"
class Pentagon : public Shape
{
public:
	Pentagon(std::string col, std::string nam, double lenght);
	virtual ~Pentagon();
	void draw();
	double CalArea();
	void setLenght(double lenght);
private:
	double _lenght;
};